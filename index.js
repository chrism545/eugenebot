const say = require('say');
const lineReader = require('readline').createInterface({
    input: require('fs').createReadStream('./sayings.txt')
});
const exec = require('child_process').exec
const google = require('google')
google.resultsPerPage = 1
var nextCounter = 0

// list of parsed strings from sayings.txt
var sayings = [];

// target person to ask a question
var target = ['Dan', 'Ryan', 'Ryan', 'Ryan', 'Chris', 'Jarrad', 'Question', 'Question', false, false, false, false, false, false]

var sayingsWithNoTarget = ['God dammit.', 'Fucking pu shee.'];

var stillSpeaking = false;

// helps avoid repeating phrases 
var thingsSaid = []; // used keep track of what was said

// say -v voice to use
const voice = 'Alex'

function getRandomIntInclusive(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive 
}

function googleOneThing( string, next ){

    console.log(`I'm googling ${ string }`)

    google( string, function( err, res ){
        if (err || !res) return console.error(err)

        var link = res.links[ 0 ];
        let final = `Interesting... ${ link.description.trim( 25 ) }`
        next( final )
    })
}

function getRandomLine() {
    var ri = getRandomIntInclusive(0, sayings.length - 1);
    if (thingsSaid.length < sayings.length - 1) {
        // pick one we have not said yet
        while (thingsSaid.indexOf(ri) !== -1)
            ri = getRandomIntInclusive(0, sayings.length - 1);
    } else {
        // reset the things said
        thingsSaid = [];
    }

    thingsSaid.push(ri);

    return sayings[ri];
}

// picks a random target
function getRandomTarget() {
    var ri = getRandomIntInclusive(0, target.length - 1);
    return target[ri];
}

// picks a target and a saying and does that
function randomSay() {

    if(stillSpeaking){
        console.log('Still speaking')

        return;
    }

    stillSpeaking = true;

    let saying = getRandomLine();
    let target = getRandomTarget();

    // random delay after calling someones name or before googling a question
    var ms = getRandomIntInclusive(100, 4000);

    // target someone as long as the saying is not in sayingsWithNoTarget
    if (target && sayingsWithNoTarget.indexOf(saying) === -1) {

        console.log('I asked: ' + target);

        // speak the targets name
        say.speak(target, voice, 1.3, () => {

            // speak the saying
            setTimeout(() => {
                console.log('I said: ' + saying);
                say.speak(saying, voice, 1.1, () => {
                    stillSpeaking = false;
                });
            }, ms);
        });
    } else { // no target, speak the saying
        console.log('I said: ' + saying);
        say.speak(saying, voice, 1.0, () => {

            // randomly google this saying and speak it
            if( getRandomIntInclusive(0, 50) % 7 === 0 ){
                setTimeout(() => {
                    googleOneThing( saying, ( answer ) => {

                        console.log('I said: ' + answer);
                        say.speak(answer, voice, 1.1, () => {
                            stillSpeaking = false;
                        });
                    })
                }, ms);
            }
            else stillSpeaking = false;
        });
    }
}

function scheduleTheJob(interval) {
    // random minute
    let int = interval || (process.env.NODE_ENV === 'development' ? 1000 * 1 : 1000 * 60)
    var ms = getRandomIntInclusive(2, 20) * int;

    console.log('next question in ' + (ms / 1000 / 60) + ' min.');

    setTimeout(() => {
        randomSay();
        scheduleTheJob();
    }, ms);
}

//////////////////////////////////////////////////////////////
///    App starts here
//////////////////////////////////////////////////////////////

// auto-update code
let updateTime = (process.env.NODE_ENV === 'development' ? 3000 : 60 * 1000)
setTimeout(() => {
    console.log('updating')

    let now = new Date()
    let hours = now.getHours()
    let mins = now.getMinutes()

    // ask about our daily standup
    if( hours === 15 && 49 < mins && mins < 59 && ! stillSpeaking ){
        let saying = 'Will there be a standup today?'
        console.log('I said: ' + saying);
        say.speak(saying, voice, 1.1, () => {
            stillSpeaking = false;
        });
    }

    exec('git pull')

}, updateTime)

// import list of sayings
lineReader.on('line', function (line) {
    var final = line.trim().replace('~~~', '').trim();
    let rand = getRandomIntInclusive(1, 100)

    if (final) {
        sayings.push(final);
        console.log('sayings.txt:', final);
    }
});

// Start the Eugenius 
lineReader.on('close', function () {

    console.log();
    console.log(sayings.length + ' Sayings');
    console.log('Eugene beguines!');

    scheduleTheJob();
});
